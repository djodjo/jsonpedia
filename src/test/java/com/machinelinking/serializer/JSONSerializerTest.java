/*
 * JSONpedia - Convert any MediaWiki document to JSON.
 *
 * Written in 2014 by Michele Mostarda <mostarda@fbk.eu>.
 *
 * To the extent possible under law, the author has dedicated all copyright and related and
 * neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC BY Creative Commons Attribution 4.0 Internationa Public License.
 * If not, see <https://creativecommons.org/licenses/by/4.0/legalcode>.
 */

package com.machinelinking.serializer;

import com.machinelinking.util.JSONUtils;
import junit.framework.Assert;
import org.codehaus.jackson.JsonNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Test case for {@link JSONSerializer}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class JSONSerializerTest {

    private ByteArrayOutputStream baos;
    private Serializer serializer;

    @Before
    public void setUp() throws IOException {
        baos = new ByteArrayOutputStream();
        serializer = new JSONSerializer(baos);
    }

    @After
    public void tearDown() {
        baos = null;
        serializer = null;
    }

    @Test
    public void testOpenObject() {
        serializer.openObject();
        verify("{}");
    }

    @Test
    public void testOpenObject2() {
        serializer.openObject();
        serializer.openObject();
        verify("{ \"@an0\" : {} }");
    }

    @Test
    public void testOpenObject3() {
        serializer.openList();
        serializer.openObject();
        verify("[{}]");
    }

    @Test
    public void testOpenObject4() {
        serializer.openList();
        serializer.field("f1");
        serializer.openObject();
        verify("[ { \"f1\" : null } , {} ]");
    }

    @Test
    public void testOpenList() {
        serializer.openList();
        verify("null");
    }

    @Test
    public void testOpenList2() {
        serializer.openList();
        serializer.openList();
        verify("[null]");
    }

    @Test
    public void testSpuriousField() {
        serializer.openList();
        serializer.field("f1");
        verify("[{ \"f1\" : null }]");
    }

    @Test
    public void testSpuriousField2() {
        serializer.openList();
        serializer.field("f1");
        serializer.field("f2");
        verify("[ { \"f1\" :  null } , { \"f2\" : null } ]");
    }

    @Test
    public void testSpuriousField3() {
        serializer.openList();
        serializer.field("f1");
        serializer.field("f2");
        serializer.value("v1");
        verify("[{ \"f1\" : null } , { \"f2\" : \"v1\" } ]");
    }

    @Test
    public void testSpuriousValue() {
        serializer.openObject();
        serializer.value("v1");
        verify("{ \"@an0\" : \"v1\" }");
    }

    @Test
    public void testSpuriousValue2() {
        serializer.openObject();
        serializer.value("v1");
        serializer.value("v2");
        verify("{ \"@an0\" : \"v1\" , \"@an1\" : \"v2\" }");
    }

    @Test
    public void testPendingObject() {
        serializer.openObject();
        serializer.field("f1");
        serializer.value("v1");
        serializer.field("f2");
        serializer.closeObject();
        verify("{ \"f1\" : \"v1\" , \"f2\" : null }");
    }

    @Test
    public void testObjectSequence() {
        serializer.openObject();
        serializer.field("f1");
        serializer.value("v1");
        serializer.field("f2");
        serializer.value("v2");
        serializer.closeObject();
        verify("{ \"f1\" : \"v1\" , \"f2\" : \"v2\" }");
    }

    @Test
    public void testListSequence() {
        serializer.openList();
        serializer.value("v1");
        serializer.value("v2");
        serializer.value("v3");
        serializer.closeList();
        verify("[ \"v1\" , \"v2\" , \"v3\" ]");
    }

    @Test
    public void testNestedSequence1() {
        serializer.openObject();
        serializer.field("f1");
        serializer.openObject();
        verify("{ \"f1\" : {} }");
    }

    @Test
    public void testNestedSequence2() {
        serializer.openObject();
        serializer.field("f1");
        serializer.openList();
        verify("{ \"f1\" : null }");
    }

    @Test
    public void testNestedSequence3() {
        serializer.openList();
        serializer.field("f1");
        serializer.openList();
        verify("[ { \"f1\" : null }, null ]");
    }

    private void verify(String expected) {
        serializer.close();
        final String out = baos.toString();
        try {
            final JsonNode expectedNode = JSONUtils.parseJSON(expected);
            final JsonNode outNode = JSONUtils.parseJSON(out);
            Assert.assertEquals(expectedNode, outNode);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
